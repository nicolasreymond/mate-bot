const
	Telegraf = require('telegraf'),
	LocalSession = require('telegraf-session-local')

const express = require('express')

const expressApp = express()



const port = process.env.PORT || 3000
expressApp.get('/', (req, res) => {
	res.send('Hello World!')
})

expressApp.listen(port, () => {
	console.log(`Listening on port ${port}`)
})

// Your Bot token here
const bot = new Telegraf(process.env.BOT_TOKEN, {
	username: 'Epfl_Mate_Bot'
})


const MATE_PRICE = 2
// Name of session property object in Telegraf Context (default: 'session')
const property = 'data'

const localSession = new LocalSession({
	// Database name/path, where sessions will be located (default: 'sessions.json')
	database: 'example_db.json',
	// Name of session property object in Telegraf Context (default: 'session')
	property: 'session',
	// Type of lowdb storage (default: 'storageFileSync')
	storage: LocalSession.storageFileAsync,
	// Format of storage/database (default: JSON.stringify / JSON.parse)
	format: {
		serialize: (obj) => JSON.stringify(obj, null, 2), // null & 2 for pretty-formatted JSON
		deserialize: (str) => JSON.parse(str),
	},
	// We will use `messages` array in our database to store user messages using exported lowdb instance from LocalSession via Telegraf Context
	state: {
		mate: []
	}
})

// Wait for database async initialization finished (storageFileAsync or your own asynchronous storage adapter)
localSession.DB.then(DB => {
	// Database now initialized, so now you can retrieve anything you want from it
	// console.log('Current LocalSession DB:', DB.value())
	// console.log(DB.get('sessions').getById('1:1').value())
})

// Telegraf will use `telegraf-session-local` configured above middleware with overrided `property` name
bot.use(localSession.middleware(property))

bot.start((ctx) => ctx.reply('Welcome to Mate bot !!\nThis bot will help our mate team to manage our stock and money.\nType /help to view starting commande\n\nenjoy and make mate great again ;)'))

bot.command('/help', (ctx) => {
	ctx.replyWithMarkdown("Here is a list of available commands... \n '/status' : Gives you how much money people have in their wallet \n '/drink' : Type this if you have drank a mate \n '/charge [ammount]' : type this if you have bought mates, and type the ammount of money you have spent.")
})

bot.on('text', (ctx, next) => {
	if (typeof ctx[property + 'DB'].get('mate').getById(ctx.chat.id).value() === 'undefined') {
		ctx[property + 'DB'].get('mate').insert({
			'id': ctx.chat.id,
			'members': []
		}).write()
	}

	console.log(ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value());

	if (!(ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value())) {
		ctx[property + 'DB'].get('mate').getById(ctx.chat.id).value().members.push(ctx.from)
		ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value().wallet = 0
	}
	return next()
})


bot.command('/status', (ctx) => {
	let members = ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').value()
	let message = ''
	members.forEach(function(value) {
		message += "@" + value.username + " → " + value.wallet + 'CHF\n';
	})
	ctx.replyWithMarkdown(message)
})

bot.command('/drink', (ctx) => {
	console.log(ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value().wallet)
	ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value().wallet -= MATE_PRICE
	let msg = `@\`${ctx.from.username || ctx.from.id}\` just drunk a mate.\n`
	msg += `New wallet status: \`${ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value().wallet}.00\` CHF`
	ctx.replyWithMarkdown(msg)
})

bot.command('/charge', (ctx) => {
	var result = ctx.message.text.match(/\/charge ([0-9\.-]+)/)
	if (!result) {
		ctx.replyWithMarkdown('Usage: /charge $, e.g. /charge 40')
	} else {
		ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value().wallet += parseInt(result[1]) //+ parseInt(ctx[property + 'DB'].get('mate').getById(ctx.chat.id+':'+ctx.from.id).value().wallet)
		let msg = `@\`${ctx.from.username || ctx.from.id}\` just charged his wallet.\n`
		msg += `New wallet status: \`${ctx[property + 'DB'].get('mate').getById(ctx.chat.id).get('members').getById(ctx.from.id).value().wallet}.00\` CHF`
		ctx.replyWithMarkdown(msg)
	}
})

bot.startPolling()
