# Mate Bot

Code d'un bot [Telegram](https://t.me) permettant de gérer la consommation de
[Club Mate](https://fr.wikipedia.org/wiki/Club-Mate) entre différentes
personnes.


# Mise en place

Afin de pouvoir tester le bot, suivez les étapes suivantes:
  1. Communiquer avec [@BotFather](https://t.me/BotFather) afin de créer un bot
  et d'obtenir le token
  1. Remplacez la clé `telegramToken` dans le fichier `secret.sample` avec le
  token du bot
  1. Installer [telegraf](https://telegraf.js.org),
  [update-json-file](https://www.npmjs.com/package/update-json-file) et [express](https://http://expressjs.com) avec la
  commande:  
    `npm i telegraf update-json-file express`
  1. Ajouter la variable d'environement `BOT_TOKEN`  
    `export BOT_TOKEN="1234567890:AABBCCDDEEFFGGHHIIJJKKLLMMNNOOPPQQR"`
  1. Lancer le script `node index.js`
  1. Parler à votre bot sur Telegram, par exemple `/people`


# Commandes

* `/start` → Welcome
* `/drink` → L'utilisateur boit un mate
* `/refill [montant]` → L'utilisateur ajoute de l'argent (par defaut 40 CHF)
* `/price` → Liste les prix courant
  * `/price set-mate 2.0`
  * `/price set-box 22.0`
* `/people` → Liste le solde des comptes pour chaques personnes
* `/leave [user]` → Le bot affiche comment solder le compte pour l'utilisateur spécifié
* `/receive user montant` → L'utilisateur courant reçoit de l'utilisateur spécifié le montant spécifié
* `/give user montant` → L'utilisateur courant donne à l'utilisateur spécifié le montant spécifié
